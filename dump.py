import applegpu
from applegpu import instruction_descriptors
import lxml.etree as ET

# Splits dst/src operand

OPERAND_SIZED = {
        "ALUDst64":   ("dst", "u", 64),
        "ALUDst":     ("dst", "u", 32),
        "ALUSrc16":   ("src", "u", 16),
        "ALUSrc64":   ("src", "u", 16),
        "ALUSrc":     ("src", "u", 32),
        "FloatDst16": ("dst", "f", 16),
        "FloatDst":   ("dst", "f", 32),
        "FloatSrc16": ("src", "f", 16),
        "FloatSrc":   ("src", "f", 32),
        "MulSrc":     ("src", "i", 32),
        "AddSrc":     ("src", "i", 64),
}

def parse_instr(o):
    ins = ET.Element('instruction')
    sz_correct = ((1 << (o.sizes[0]*8)) - 1)
    ins.set('name', o.name)
    ins.set('mask', hex(o.mask & sz_correct))
    ins.set('bits', hex(o.bits & sz_correct))

    if o.sizes[0] != o.sizes[1]:
        ins.set('size_1', str(o.sizes[0]))
        ins.set('size_2', str(o.sizes[1]))
        ins.set('length_bit_pos', str(o.length_bit_pos))
    else:
        ins.set('size', str(o.sizes[0]))

    for (name, subs) in o.merged_fields:
        merged = ET.SubElement(ins, 'field')
        merged.set('name', name)

        if len(subs) == 1:
            assert(subs[0][0] == name)
            assert(subs[0][1] == 0)
            lookup = [x for x in o.fields if x[2] == name]
            assert(len(lookup) == 1)
            (start, size, _) = lookup[0]
            merged.set('start', str(start))
            merged.set('size', str(size))
        else:
            for (subname, shift) in subs:
                # Lookup the underlying field
                lookup = [x for x in o.fields if x[2] == subname]
                assert(len(lookup) == 1)
                (start, size, _) = lookup[0]

                el = ET.SubElement(merged, 'sub')
                el.set('name', subname)
                el.set('start', str(start))
                el.set('size', str(size))
                el.set('shift', str(shift))

    for p in o.ordered_operands:
        typ = type(p).__name__[:-len('Desc')]

        if typ in OPERAND_SIZED:
            (tag, op_type, max_bits) = OPERAND_SIZED[typ]
            op = ET.SubElement(ins, tag)
            op.set('name', p.name)
            op.set('max_bits', str(max_bits))
            op.set('type', op_type)
        elif typ in ["MemoryShift", "Shift"]:
            op = ET.SubElement(ins, 'shift')
            op.set('name', 's')
        elif typ == "Immediate":
            op = ET.SubElement(ins, 'immediate')
            op.set('name', p.name)
        elif typ == "MemoryReg":
            op = ET.SubElement(ins, 'memory_reg')
            op.set('name', p.name)
        elif typ == "MemoryIndex":
            op = ET.SubElement(ins, 'memory_index')
            op.set('name', p.name)
        elif p.name == "mask":
            op = ET.SubElement(ins, 'mask')
            op.set('name', p.name)
        elif p.name == "F":
            op = ET.SubElement(ins, 'format')
            op.set('name', p.name)
        else:
            op = ET.SubElement(ins, 'operand')
            op.set('name', p.name)
            op.set('type', typ)

    return ins

document = ET.Element('agx')

for o in instruction_descriptors:
    document.append(parse_instr(o))

ET.dump(document)
